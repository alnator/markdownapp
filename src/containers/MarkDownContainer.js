import React, {Component} from 'react';
import {Rows, Columns, Cell, Splitter} from 'react-resizable-grid';
import cookie from 'react-cookies'
import marked from 'marked';
import SplitPane from 'react-split-pane';
import io from 'socket.io-client';

import EditorPanel from '../components/EditorPanel';
import PreviewPanel from '../components/PreviewPanel';

class MarkDownContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      markdown: "",
      html: ""
    }

    //bindings
    this.ConvertMarkDown = this.ConvertMarkDown.bind(this);
    this.UpdateMarkDown = this.UpdateMarkDown.bind(this);

    this.socket = io("http://localhost:4000");
  }



  componentWillMount() {

    let markdown = cookie.load('markdown');
    console.log(markdown);
    if (typeof markdown != "undefined") {
      this.setState({
        markdown: markdown,
        html: ""
      });
    }

  }

  componentDidMount(){



    this.socket.on('result' , (result) => {
      try {
        let updateHtml = {
          html: result.join('<br/>')
        };
        this.setState(updateHtml);
      } catch (err) {
        console.log(`Opps ${err}`)
      }
    });



  }

  UpdateMarkDown(e) {

    try {

      let newMarkDown = e.target.value;

      let updateMarkDown = {
        markdown: newMarkDown,
      };

      this.setState(updateMarkDown);
      cookie.save('markdown', newMarkDown, {path: '/'});



    } catch (err) {
      console.log(`Opps ${err}`)
    }
  }

  ConvertMarkDown(e) {
    this.socket.emit('code',this.state.markdown);
  }
  render() {
    return (
      <div className="editor">
        <button onClick={this.ConvertMarkDown}>Run</button>
        <hr/>
        <div>
          <SplitPane split="vertical" minSize={400} defaultSize={Math.floor(window.innerWidth/2)}>
            <EditorPanel markdown={this.state.markdown} onUpdateMarkDown={this.UpdateMarkDown}/>
            <PreviewPanel html={this.state.html}/>
          </SplitPane>
        </div>


      </div>
    );
  }
}

export default MarkDownContainer;
