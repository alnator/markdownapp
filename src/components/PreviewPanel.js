import React, { Component } from 'react';

// i kept this module , if you like to look more in to it , becouse you liked it .
import renderHTML from 'react-render-html';

const PreviewPanel = ({html}) => (
  <div className="preview-panel">
      {renderHTML(html)}
  </div>
)

export default PreviewPanel;
