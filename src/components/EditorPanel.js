import React, { Component } from 'react';

const EditorPanel = ({onUpdateMarkDown,markdown}) => (
  <div className="editor-panel">
    <textarea onChange={onUpdateMarkDown} value={markdown}></textarea>
  </div>
)

export default EditorPanel;
