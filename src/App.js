import React, { Component } from 'react';
import MarkDownContainer from './containers/MarkDownContainer';
import './App.css';

class App extends Component {

  render() {

    return (
      <div className="App">
          <MarkDownContainer />
      </div>
    );
  }
}



export default App;
