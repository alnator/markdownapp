const express = require('express');
const http = require('http');
const app = express();
const server = http.createServer(app);
const io = require('socket.io').listen(server);
const PythonShell = require('python-shell');
const fs = require('fs');

// PythonShell Options
var options = {
  scriptPath: __dirname + '/../scripts'
};

// you can have a socket.io cluster for scaling also : https://socket.io/docs/using-multiple-nodes/


// On Socket Connect
io.on('connection', function(socket) {

  console.log(`connected as ${socket.id}`);

  socket.on('code', function(code) {

    let scriptFile = `${socket.id}.py`;
    let scriptFileFullPath = `${__dirname}/../scripts/${scriptFile}`;

    // Write a Python File
    fs.writeFile(scriptFileFullPath, code, function(err) {
      if (err) {
        return console.log(err);
      }

      console.log("The file was saved!");

      //Run The Python File and Send Back the result
      PythonShell.run(scriptFile, options, function(err, result) {

        // TODO : Better Error Handling to be able to display an error on the UI
        if (err)
          throw err;

        console.log(result);
        socket.emit('result', result);

        console.log('finished');
      });

    });

  });
});

server.listen(4000);
